package scheduler.model;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private  String name;
    private Trainer trainer;
    private List<Student> studentList;

    public Group(String name, Trainer trainer) {
        this.name = name;
        this.trainer = trainer;
        this.studentList = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    @Override
    public String toString() {
        return "Group{" +
                "name='" + name + '\'' +
                ", trainer=" + trainer +
                ", studentList=" + studentList +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public void addStudent(Student student){
        studentList.add(student);
    }
}
