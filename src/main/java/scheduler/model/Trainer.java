package scheduler.model;

import java.util.Date;

public class Trainer extends Person {

    private boolean isAuthorized;
    private boolean busy;

    public Trainer(String firstName, String lastName, Date dateOfBirth, boolean isAuthorized) {
        super(firstName, lastName, dateOfBirth);
        this.isAuthorized = isAuthorized;
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }
}
