package scheduler;

import scheduler.model.Group;
import scheduler.model.Student;
import scheduler.model.Trainer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class SDAScheduler {

    private List<Student> studentList;
    private List<Trainer> trainerList;
    private List<Group> groupList;

    public SDAScheduler() {
        studentList = new ArrayList<>();
        trainerList = new ArrayList<>();
        groupList = new ArrayList<>();
    }

    public void addStudent(String firstName, String lastName, String dateOfBirth, boolean hasPreviousJavaKnowledge) throws ParseException {
        Date dateOfBirth1 = new SimpleDateFormat("dd.mm.yyyy").parse(dateOfBirth);
        Student e = new Student(firstName, lastName, dateOfBirth1, hasPreviousJavaKnowledge);
        studentList.add(e);
    }

    public void addTrainer(String firstName, String lastName, Date dateOfBirth, boolean isAuthorised) {
        trainerList.add(new Trainer(firstName,lastName,dateOfBirth,isAuthorised));
    }

    public void addGroup(String name) {
        groupList.add(new Group(name,null));
    }

    public boolean readyToAssign(){
        return studentList.size() > 0 && trainerList.size() > 0 && groupList.size() > 0;
    }

    public boolean assignGroups(){
        if(readyToAssign()){
            for (Group group : groupList) {
                Trainer trainer = getTrainer();
                trainer.setBusy(true);
                group.setTrainer(trainer);


                int groupSize = 3;
                for (int i = 0; i < groupSize; i++) {
                    Student s = getStudent();
                    if(s == null){
                        break;
                    }
                    s.setBusy(true);
                    group.addStudent(s);
                }

            }
            System.out.println(groupList);
            return true;
        }else {
            return false;
        }
    }

    private Student getStudent() {
        Optional<Student> first = studentList.stream()
                .filter(el -> !el.isBusy())
                .findFirst();
        return first.orElse(null);
    }

    private Trainer getTrainer() {
        return trainerList.stream()
                .filter(el-> !el.isBusy())
                .findFirst().get();
    }

    public List<Group> getGroupList() {
        return groupList;
    }
}
