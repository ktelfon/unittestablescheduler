import java.util.ArrayList;
import java.util.List;

public class Zoo {

    private List<String> visitors = new ArrayList<>();

    public void addVisitor(String name){
        if(visitors.size() < 2){
            visitors.add(name);
        }
    }

    public int visitorCount(){
        return visitors.size();
    }
}
