package scheduler;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class SDASchedulerTest {

    SDAScheduler sdaScheduler;

    @BeforeEach
    void beforeEach(){
        sdaScheduler = new SDAScheduler();
    }

    @Test
    void addStudent() {
        assertThrows(ParseException.class, () -> {
            sdaScheduler.addStudent("asdf",
                    "", "1123.2323",
                    true);

        });
    }

    @Test
    void assignStudents() throws ParseException {
        sdaScheduler.addStudent(
                "String firstName",
                "String lastName",
                "11.03.1989",
                true);

        sdaScheduler.addTrainer(
                "String firstName",
                "String lastName",
                new Date(),
                true);

        sdaScheduler.addGroup("String firstName");

        sdaScheduler.assignGroups();

        assertEquals(1,sdaScheduler.getGroupList().size());
    }
}