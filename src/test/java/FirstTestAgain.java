import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class FirstTestAgain {
    private final Zoo zoo = new Zoo();
    @Test
    void testZoo(){
        zoo.addVisitor("janis");
        zoo.addVisitor("mike");
        zoo.addVisitor("jim");

        assertEquals(2, zoo.visitorCount());
    }
}
